/*
 * Marker_util.h
 *
 *  Created on: Apr 3, 2012
 *      Author: somervil
 *
 * COPYRIGHT (C) 2005-2012
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2012 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef RE2_MARKER_UTIL_H_
#define RE2_MARKER_UTIL_H_

#include <visualization_msgs/Marker.h>
#include <geometry_msgs/Point.h>

namespace re2
{ namespace visualization_msgs_util
{

    template< typename Vector3T >
    void appendPointToMarkerMsg( const Vector3T & point3, visualization_msgs::MarkerPtr & msg )
    {
        geometry_msgs::Point point;

        point.x = point3[0];
        point.y = point3[1];
        point.z = point3[2];

        msg->points.push_back( point );
    }


    template< typename Vector4T >
    visualization_msgs::MarkerPtr
    createMarkerMsg( int                id,
                     int32_t            type,
                     const Vector4T &   color,
                     const std::string& ns,
                     const std::string& frameId  )
    {
        visualization_msgs::MarkerPtr markerMsg( new visualization_msgs::Marker );

        markerMsg->header.frame_id = frameId;
        markerMsg->header.stamp    = ros::Time::now();
        markerMsg->ns              = ns;
        markerMsg->id              = id;
        markerMsg->type            = type;
        markerMsg->action          = visualization_msgs::Marker::MODIFY;
        markerMsg->frame_locked    = false; //true;
        markerMsg->scale.x         = 0.01;
        markerMsg->scale.y         = 0.01;
        markerMsg->scale.z         = 0.01;
        markerMsg->color.r         = color[0];
        markerMsg->color.g         = color[1];
        markerMsg->color.b         = color[2];
        markerMsg->color.a         = color[3];
        markerMsg->lifetime        = ros::Duration( 0  );

        return markerMsg;
    }



    template< typename Vector4T >
    visualization_msgs::MarkerPtr
    createMarkerMsg( int                id,
                     int32_t            type,
                     const Vector4T &   color,
                     const std::string& ns     )
    {
        return createMarkerMsg( id,
                                type,
                                color,
                                ns,
                                "/world" );
    }


    template< typename Vector4T >
    visualization_msgs::MarkerPtr
    createPointsMsg( int                id,
                     const Vector4T &   color,
                     const std::string& ns,
                     const std::string& frameId  )
    {
        return createMarkerMsg( id,
                                visualization_msgs::Marker::POINTS,
                                color,
                                ns,
                                frameId );
    }


    template< typename Vector4T >
    visualization_msgs::MarkerPtr
    createPointsMsg( int                id,
                     const Vector4T &   color,
                     const std::string& ns     )
    {
        return createPointsMsg( id,
                                color,
                                ns,
                                "/world" );
    }


}
}

#endif /* MARKER_UTIL_H_ */
