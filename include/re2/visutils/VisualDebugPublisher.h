/*
 * VisualDebugPublisher.h
 *
 *  Created on: Nov 2, 2011
 *      Author: somervil
 *
 * COPYRIGHT (C) 2005-2012
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2012 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef RE2_VISUALDEBUGPUBLISHER_H_
#define RE2_VISUALDEBUGPUBLISHER_H_

//#include <re2/marker_util.h>
#include <re2/visutils/marker_util.h>
#include <re2/eigen/eigen_util.h>
#include <visualization_msgs/Marker.h>
#include <ros/ros.h>
#include <boost/functional/hash.hpp>
#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>
#include <string>

namespace re2
{

class VisualDebugPublisher
{
    public:
        typedef boost::shared_ptr<VisualDebugPublisher> Ptr;

    public:
        VisualDebugPublisher( const std::string & topic, const std::string & frameName = std::string( "/world" ) )
        {
            m_markerPub = m_node.advertise<visualization_msgs::Marker>( topic, 100 );

            m_node.getParam(               "tf_prefix", m_tfPrefix );
            ros::NodeHandle("~").getParam( "tf_prefix", m_tfPrefix );

            if(  !m_tfPrefix.empty()
               && m_tfPrefix[m_tfPrefix.size()-1] != '/' )
                m_tfPrefix += "/";

            m_frameName = m_tfPrefix + frameName;
        }


        void setDefaultFrame( const std::string & newFrame )
        {
            m_frameName = m_tfPrefix + newFrame;
        }


        template< typename PointT, typename ColorT >
        void publishPoint3( const PointT & point3, const ColorT & color, int id = 0, std::string frameName = std::string(), std::string groupName = "debug" )
        {
            using namespace visualization_msgs_util;

            visualization_msgs::MarkerPtr markerMsg;

            if( frameName.empty() )
                frameName = m_frameName;

            markerMsg = createPointsMsg( id, color, groupName, frameName );

            appendPointToMarkerMsg( point3, markerMsg );

            m_markerPub.publish( markerMsg );
        }

        template< typename PointT, typename ColorT >
        void publishTextAt( const std::string & text, double scale, const PointT & point3, const ColorT & color, int id = 0, std::string frameName = std::string() )
        {
            using namespace visualization_msgs_util;

            visualization_msgs::MarkerPtr markerMsg;

            if( frameName.empty() )
                frameName = m_frameName;

            markerMsg = createPointsMsg( id, color, "debug", frameName );
            markerMsg->type = visualization_msgs::Marker::TEXT_VIEW_FACING;
            markerMsg->text = text;
            markerMsg->scale.z = scale;
            Eigen::Vector3dMap( &markerMsg->pose.position.x ) = point3;

            m_markerPub.publish( markerMsg );
        }

        template< typename PointT>
        void publishPoint3( const PointT & point3 )
        {
            publishPoint3( point3, Eigen::Vector4d( 0.5,0.5,0.5,1) );
        }

        template< typename PointT, typename ColorT >
        void publishScaledPoint3( const PointT & point3, const ColorT & color, double scale, int id = 0, std::string frameName = std::string() )
        {
            using namespace visualization_msgs_util;

            visualization_msgs::MarkerPtr markerMsg;

            if( frameName.empty() )
                frameName = m_frameName;

            markerMsg = createPointsMsg( id, color, "debug", frameName );

            appendPointToMarkerMsg( point3, markerMsg );
            markerMsg->scale.x = scale;
            markerMsg->scale.y = scale;
            markerMsg->scale.z = scale;

            m_markerPub.publish( markerMsg );
        }


        template< typename Point3VecT, typename ColorT >
        void publishPolygon3( const Point3VecT & point3vec, const ColorT & color, int id = 0 )
        {
            using namespace visualization_msgs_util;

            typedef typename Point3VecT::value_type Point3T;

            visualization_msgs::MarkerPtr markerMsg;
            markerMsg = createMarkerMsg( id, visualization_msgs::Marker::LINE_STRIP, color,  "debug", m_frameName );

            BOOST_FOREACH( Point3T point3, point3vec )
                appendPointToMarkerMsg( point3, markerMsg );

            m_markerPub.publish( markerMsg );
        }


        template< typename SegmentVecT, typename ColorT >
        void publishSegments( const SegmentVecT & segments, const ColorT & color, int id = 0 )
        {
            using namespace visualization_msgs_util;

            typedef typename SegmentVecT::value_type SegmentT;

            visualization_msgs::MarkerPtr markerMsg;
            markerMsg = createMarkerMsg( id, visualization_msgs::Marker::LINE_LIST, color,  "debug", m_frameName );

            BOOST_FOREACH( SegmentT segment, segments )
            {
                appendPointToMarkerMsg( segment.p0(), markerMsg );
                appendPointToMarkerMsg( segment.p1(), markerMsg );
            }

            m_markerPub.publish( markerMsg );
        }


        template< typename SegmentT, typename ColorT >
        void publishSegment( const SegmentT & segment, const ColorT & color, int id = 0, std::string frameName = std::string()  )
        {
            using namespace visualization_msgs_util;

            if( frameName.empty() )
                frameName = m_frameName;

            visualization_msgs::MarkerPtr markerMsg;
            markerMsg = createMarkerMsg( id, visualization_msgs::Marker::LINE_LIST, color,  "debug", frameName );

            appendPointToMarkerMsg( segment.p0(), markerMsg );
            appendPointToMarkerMsg( segment.p1(), markerMsg );

            m_markerPub.publish( markerMsg );
        }

        template< typename VectorT0, typename VectorT1,  typename ColorT >
        void publishSegment( const VectorT0 & p0, const VectorT1 & p1, const ColorT & color, int id = 0, std::string frameName = std::string()  )
        {
            using namespace visualization_msgs_util;

            if( frameName.empty() )
                frameName = m_frameName;

            visualization_msgs::MarkerPtr markerMsg;
            markerMsg = createMarkerMsg( id, visualization_msgs::Marker::LINE_LIST, color,  "debug", frameName );

            appendPointToMarkerMsg( p0, markerMsg );
            appendPointToMarkerMsg( p1, markerMsg );

            m_markerPub.publish( markerMsg );
        }


        template< typename LineT, typename ColorT >
        void publishLine( const LineT & line, double length, const ColorT & color, int id = 0 )
        {
            using namespace visualization_msgs_util;

            visualization_msgs::MarkerPtr markerMsg;
            markerMsg = createMarkerMsg( id, visualization_msgs::Marker::LINE_LIST, color,  "debug", m_frameName );

            appendPointToMarkerMsg( line.origin(), markerMsg );
            appendPointToMarkerMsg( line.origin() + line.direction().normalized()*length, markerMsg );

            m_markerPub.publish( markerMsg );
        }


        template< typename VectorT0, typename VectorT1, typename ColorT >
        void publishVectorFrom( const VectorT0 & origin, const VectorT1 & direction, const ColorT & color, int id = 0, std::string frameName = std::string(), std::string group = std::string() )
        {
            using namespace visualization_msgs_util;

            if( frameName.empty() )
                frameName = m_frameName;

            if( group.empty() )
                group = "debug";

            visualization_msgs::MarkerPtr markerMsg;
            markerMsg = createMarkerMsg( id, visualization_msgs::Marker::LINE_LIST, color,  group, frameName );

            appendPointToMarkerMsg( origin, markerMsg );
            appendPointToMarkerMsg( origin + direction, markerMsg );
//            markerMsg->text = id;

            m_markerPub.publish( markerMsg );
        }

        template< typename VectorT, typename ColorT >
        void publishVector( const VectorT & direction, const ColorT & color, int id = 0, std::string frameName = std::string() )
        {
            publishVectorFrom( Eigen::Vector3d(0,0,0), direction, color, id, frameName );
        }

        template< typename VectorT  >
        void publishVector( const VectorT & direction )
        {
            publishVectorFrom( Eigen::Vector3d(0,0,0), direction, Eigen::Vector4d( 0.5,0.5,0.5,1)  );
        }


        template< typename SegmentVecT, typename ColorT >
        void publishSegmentWalls( const SegmentVecT & segments, const ColorT & color, int id = 0 ) // FIXME this should be moved out
        {
            using namespace visualization_msgs_util;

            typedef typename SegmentVecT::value_type SegmentT;

            visualization_msgs::MarkerPtr markerMsg;
            markerMsg = createMarkerMsg( id, visualization_msgs::Marker::LINE_STRIP, color,  "debug", m_frameName );

            BOOST_FOREACH( SegmentT segment, segments )
            {
                appendPointToMarkerMsg( segment.p0(),           markerMsg );
                appendPointToMarkerMsg( segment.p0() + zAxis3d, markerMsg );
                appendPointToMarkerMsg( segment.p1() + zAxis3d, markerMsg );
                appendPointToMarkerMsg( segment.p1(),           markerMsg );
                appendPointToMarkerMsg( segment.p0(),           markerMsg );
            }

            m_markerPub.publish( markerMsg );
        }

    protected:
        ros::NodeHandle  m_node;
        ros::Publisher   m_markerPub;
        std::string      m_frameName;
        std::string      m_tfPrefix;
};


}


#endif /* RE2_VISUALDEBUGPUBLISHER_H_ */
